# LibreWave ECP5 SoM 
An FPGA System-on-Module based on the Lattice ECP5 FPGA. With its full compatibility with the [SatNOGS COMMS system](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware) this board is designed to serve as a low-power DSP module that is drop-in compatible with the AC7Z020 Xilinx Zynq SoM originally used in SatNOGS COMMS.

## Documentation
Although much information can be found in the issues and although the majority of it is up-to-date, the official documentation of the project can be found in the [LibreWave Documentation repository](https://gitlab.com/librespacefoundation/librewave-docs). The documents there are the ones that should be trusted as the source of truth regarding the enginnering decisions made. The schematic and PCB files on GitLab provide the most recent status of the board and may sometimes be ahead of the documentation.

## 3D Models
To avoid extensive licencing precautions as well as creating and uploading many 3D models, in this project the decision has been made to use placeholder 3D models of similar external dimensions for the non-critical parts of the board. For example, the DC-DC switching converter, the flash memory IC and the FPGA use 3D models with wrong pitch and number of pins, but of correct outer dimensions. Of course, connectors and interface components must have their proper 3D models to represent reality.

